package com.einavapps.mapfragmentapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import com.einavapps.mapfragmentapp.R;

import java.util.ArrayList;

public class WaitingIndicator extends LinearLayout {

    private static final int ANIMATION_DURATION = 250;
    private static final int DELAY_BETWEEN_ANIMATED_VIEWS = 120;
    private final int mViews[] = {R.id.waiting_indicator_1, R.id.waiting_indicator_2, R.id.waiting_indicator_3};
    private int finalDelay = ANIMATION_DURATION * 2 + DELAY_BETWEEN_ANIMATED_VIEWS * (mViews.length - 2);

    private CompositeLinearAnimatorRunner lastCompositeLinearAnimatorRunner;

    public WaitingIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void startWaitingAnimation() {
        stopWaitingAnimation();
        createAnimationsRunnerAndStartAnimation();
    }

    public boolean isWaitingAnimationActive() {
        CompositeLinearAnimatorRunner animatorRunner = lastCompositeLinearAnimatorRunner;
        return ((animatorRunner != null) && animatorRunner.isActive());
    }

    private void createAnimationsRunnerAndStartAnimation() {
        ArrayList<CompositeLinearAnimator> compositeAnimations = new ArrayList<>();
        for (int i = 0; i < mViews.length; i++) {
            compositeAnimations.add(generateCompositeAnimation(this.findViewById(mViews[i])));
        }
        lastCompositeLinearAnimatorRunner = new CompositeLinearAnimatorRunner(compositeAnimations, finalDelay, this);
        lastCompositeLinearAnimatorRunner.startAnimationWithDelay(0);
    }

    private CompositeLinearAnimator generateCompositeAnimation(View view) {
        Animation anArray[] = {buildScaleAnimation(1.0f, 1.5f), buildScaleAnimation(1.5f, 1.0f)};
        CompositeLinearAnimator ca = new CompositeLinearAnimator(view, anArray);
        return ca;
    }


    public void stopWaitingAnimation() {
        if (lastCompositeLinearAnimatorRunner != null) {
            lastCompositeLinearAnimatorRunner.cancelCompositeLinearAnimatorRunner();
            lastCompositeLinearAnimatorRunner = null;
        }
    }

    private Animation buildScaleAnimation(float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setDuration(ANIMATION_DURATION);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());//new ParabulaInterpolator());
        anim.setFillAfter(true); // Needed to keep the result of the animation
        return anim;
    }

    private static class CompositeLinearAnimatorRunner {
        private ArrayList<CompositeLinearAnimator> compositeAnimationsToRun;
        private boolean isCompositeLinearAnimatorRunnerCanceled = false;
        private View parrentView;
        private int currentlyRunningAnimator = 0;
        private int finalDelay;

        public CompositeLinearAnimatorRunner(ArrayList<CompositeLinearAnimator> compositeAnimationsToRun, int finalDelay, View parrentView) {
            this.compositeAnimationsToRun = compositeAnimationsToRun;
            this.finalDelay = finalDelay;
            this.parrentView = parrentView;
        }

        private void startAnimationWithDelay(int delay) {
            if (!isCompositeLinearAnimatorRunnerCanceled) {
                parrentView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        CompositeLinearAnimator compositeAnimation = compositeAnimationsToRun.get(currentlyRunningAnimator);
                        if (!isCompositeLinearAnimatorRunnerCanceled && !compositeAnimation.isAnimationCancelled) {
                            currentlyRunningAnimator++;
                            compositeAnimation.startCompositAnimation();
                            if (currentlyRunningAnimator < compositeAnimationsToRun.size()) {
                                startAnimationWithDelay(DELAY_BETWEEN_ANIMATED_VIEWS);
                            } else {
                                currentlyRunningAnimator = 0;
                                startAnimationWithDelay(finalDelay);
                            }
                        }
                    }
                }, delay);
            }

        }

        public void cancelCompositeLinearAnimatorRunner() {
            isCompositeLinearAnimatorRunnerCanceled = true;
            for (int i = 0; i < compositeAnimationsToRun.size(); i++) {
                CompositeLinearAnimator compositeAnimation = compositeAnimationsToRun.get(i);
                compositeAnimation.cancelAnimation();
            }
        }

        public boolean isActive() {
            return !isCompositeLinearAnimatorRunnerCanceled;
        }
    }

    private static class CompositeLinearAnimator {
        private View animatedView;
        private Animation[] animations;
        int currentAnimation = 0;
        boolean isAnimationCancelled = false;

        public CompositeLinearAnimator(View animatedView, Animation[] animations) {
            this.animatedView = animatedView;
            this.animations = animations;

        }

        public void startCompositAnimation() {
            currentAnimation = 0;
            cancelAnimation();
            isAnimationCancelled = false;
            startAtomicAnimation();
        }

        private void startAtomicAnimation() {
            if (!isAnimationCancelled && animations.length > currentAnimation) {
                Animation anim = animations[currentAnimation++];
                animatedView.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        startAtomicAnimation();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }

        public void cancelAnimation() {
            isAnimationCancelled = true;
            for (Animation anim : animations) {
                anim.setAnimationListener(null);
                anim.cancel();
            }
            animatedView.clearAnimation();
            animatedView.setAnimation(null);
        }
    }
}

