package com.einavapps.mapfragmentapp.common;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.data.PlacesContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

public class RetrivePlacesIntentService extends IntentService {

    private static final String LOG_TAG = RetrivePlacesIntentService.class.getSimpleName();
    private static final String API_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=";
    private static final String API_KEY = "AIzaSyCgKsjYNr1jQWL2rPhzK7Fr6uZW-D8k6IU";//AIzaSyCjKW2S0NRFtxB6H8hPSnSxOHBldyf8tFg";//AIzaSyCgKsjYNr1jQWL2rPhzK7Fr6uZW-D8k6IU";
    private static final String BASE_ICON_REFERENCE = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=800&photoreference=";

    public RetrivePlacesIntentService() {
        super("RetrivePlacesIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        boolean isUseLocation;
        String query = intent.getStringExtra("query");
        String urlString;
        isUseLocation = intent.getBooleanExtra("is_use_location", false);
        String latlng = intent.getStringExtra("latlng");
        urlString = API_URL + query;
        if (isUseLocation && latlng != null) {
             urlString += "&location=" + latlng + "&radius=" + "1000" + "&key=" + API_KEY;
        } else {
            urlString += "&key=" + API_KEY;
        }

        HttpURLConnection httpCon = null;
        InputStream input_stream = null;
        InputStreamReader input_stream_reader = null;
        BufferedReader input = null;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlString);
            httpCon = (HttpURLConnection) url.openConnection();
            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return;
            }

            input_stream = httpCon.getInputStream();
            input_stream_reader = new InputStreamReader(input_stream);
            input = new BufferedReader(input_stream_reader);
            String line;
            while ((line = input.readLine()) != null) {
                response.append(line + "\n");
            }

            if (response.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            getResultsFromJson(response.toString());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input_stream_reader.close();
                    input_stream.close();
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                httpCon.disconnect();
            }
        }
    }


    private void getResultsFromJson(String s) {
        try {
            JSONObject object;
            if (s != null && !s.isEmpty()) {
                object = new JSONObject(s);
                JSONArray resultsJsonArray = object.getJSONArray("results");
                Vector<ContentValues> cVVector = new Vector<>(resultsJsonArray.length());
                for (int i = 0; i < resultsJsonArray.length(); i++) {
                    JSONObject item = resultsJsonArray.getJSONObject(i);
                    String name = item.getString("name");
                    JSONArray types = item.getJSONArray("types");
                    String type = "";
                    for (int j = 0; j < 2; j++) {
                        type += types.getString(j) + ", ";
                    }
                    type = type.substring(0, type.length() - 2);
                    String address = item.getString("formatted_address");
                    String placeId = item.getString("place_id");
                    if(isAlreadyExistsInDataBase(placeId)){
                        break;
                    }
                    JSONObject geometry = item.getJSONObject("geometry");
                    JSONObject location = geometry.getJSONObject("location");
                    double latitude = location.getDouble("lat");
                    double longitude = location.getDouble("lng");
                    int isOpenNow = -1;
                    if (item.has("opening_hours")) {
                        JSONObject openingHours = item.getJSONObject("opening_hours");
                        boolean isOpen = openingHours.getBoolean("open_now");
                        if (isOpen) {
                            isOpenNow = 1;
                        } else {
                            isOpenNow = 0;
                        }
                    }
                    double rating = 0;
                    if (item.has("rating")) {
                        rating = item.getDouble("rating");
                    }
                    String iconReference;
                    if (item.has("photos")) {
                        JSONArray photos = item.getJSONArray("photos");
                        JSONObject photo = photos.getJSONObject(0);
                        iconReference = BASE_ICON_REFERENCE  + photo.getString("photo_reference") + getString(R.string.key) + API_KEY;
                    } else {
                        iconReference = "null";
                    }
                    ContentValues searchResultsValues = new ContentValues();
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_ID, placeId);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_NAME, name);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_TYPE, type);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_ADDRESS, address);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_ICON_REFERENCE, iconReference);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_IS_OPEN, isOpenNow);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_RATING, rating);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_COORD_LAT, latitude);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_COORD_LONG, longitude);
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE, "0");
                    searchResultsValues.put(PlacesContract.SearchResultsEntry.COLUMN_IS_HISTORY, "1");

                    cVVector.add(searchResultsValues);
                }
                int inserted = 0;
                // add to database
                if (cVVector.size() > 0) {
                    ContentValues[] cvArray = new ContentValues[cVVector.size()];
                    cVVector.toArray(cvArray);
                    inserted = getContentResolver().bulkInsert(PlacesContract.SearchResultsEntry.CONTENT_URI, cvArray);
                    getContentResolver().notifyChange(PlacesContract.SearchResultsEntry.CONTENT_URI, null);
                }
                Log.d(LOG_TAG, "RetrivePlacesIntentService Complete. " + inserted + " Inserted");

            }


        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    /**
     * check if the given id already exists in data base
     *
     * @param id - the id by which to check
     * @return true if already saved, false otherwise
     */
    public boolean isAlreadyExistsInDataBase(String id) {
        Cursor retCursor = getContentResolver().query(PlacesContract.SearchResultsEntry.CONTENT_URI, null, null, null, null);
        boolean isSaved = false;
        if (retCursor != null && retCursor.moveToFirst()) {
            retCursor.moveToFirst();
            do {
                String currentId = retCursor.getString(1);
                if (currentId.equals(id)) {
                    isSaved = true;
                    break;
                }
            } while (retCursor.moveToNext());
        } else {
            isSaved = false;
        }
        if(retCursor != null){
            retCursor.close();
        }
        return isSaved;
    }
}
