package com.einavapps.mapfragmentapp.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.view.Window;
import android.widget.FrameLayout;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.data.PlacesContract;
import com.einavapps.mapfragmentapp.pojo.Place;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Utility {

    private static final int COL_NUM_PLACE_ID = 1;
    private static final int COL_NUM_PLACE_NAME = 2;
    private static final int COL_NUM_PLACE_TYPE = 3;
    private static final int COL_NUM_PLACE_ADDRESS = 4;
    private static final int COL_NUM_PLACE_ICON_REFERENCE = 5;
    private static final int COL_NUM_PLACE_IS_OPEN = 6;
    private static final int COL_NUM_RATING = 7;
    private static final int COL_NUM_PLACE_COORD_LAT = 8;
    private static final int COL_NUM_PLACE_COORD_LONG = 9;
    private static final int COL_NUM_IS_FAVORITE= 10;
    private static final int COL_NUM_IS_HISTORY= 11;

    public static ArrayList<Place> cursorToArrayList(Cursor cur) {
        ArrayList<Place> places = null;
        if (cur != null && cur.moveToFirst()) {
            places = new ArrayList<>();
            cur.moveToFirst();
            do {
                String id = cur.getString(COL_NUM_PLACE_ID);
                String name = cur.getString(COL_NUM_PLACE_NAME);
                String type = cur.getString(COL_NUM_PLACE_TYPE);
                String address = cur.getString(COL_NUM_PLACE_ADDRESS);
                String iconReference = cur.getString(COL_NUM_PLACE_ICON_REFERENCE);
                int open = cur.getInt(COL_NUM_PLACE_IS_OPEN);
                double rating = cur.getDouble(COL_NUM_RATING);
                double latitude = cur.getDouble(COL_NUM_PLACE_COORD_LAT);
                double longitude = cur.getDouble(COL_NUM_PLACE_COORD_LONG);
                int isFavorite = cur.getInt(COL_NUM_IS_FAVORITE);
                int isHistory = cur.getInt(COL_NUM_IS_HISTORY);
                places.add(new Place(id, name, type, address, iconReference, open, rating,
                        latitude, longitude, isFavorite, isHistory));
            }
            while (cur.moveToNext());

        }
        return places;
    }


    public static double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
        int r = 6371; // average radius of the earth in km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                        * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return r * c;
    }


    public static String changeDistanceUnits(float distance, Context c) {
        String distanceUnit;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(c);
        boolean isKilometers = sharedPref.getString(c.getResources().getString(R.string.pref_units_key), "metric").equals("metric");

        if (!isKilometers) {
            distance *= 0.621371192F;
            distance = Float.valueOf(new DecimalFormat("#.#").format(distance));
            distanceUnit = " " + c.getResources().getString(R.string.miles);
        } else if (distance > 1.0) {
            distance = Float.valueOf(new DecimalFormat("#.#").format(distance));
            distanceUnit = " " + c.getResources().getString(R.string.km);
        } else {
            distance *= 1000;
            distanceUnit = " " + c.getResources().getString(R.string.m);
            return (int) distance + distanceUnit;
        }
        return distance + distanceUnit;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    /**
     * checks weather two pane mode is on
     * @return weather two pane mode is on
     */
    public static boolean isTwoPaneMode(Activity activity) {
        return activity.getResources().getBoolean(R.bool.isTwoPane);
    }

    /**
     * check if the given id already exists in data base
     *
     * @param id - the id by which to check
     * @return true if already saved, false otherwise
     */
    public static boolean isAlreadyExistsInDataBase(String id, Context context) {
        Cursor retCursor = context.getContentResolver().query(PlacesContract.SearchResultsEntry.CONTENT_URI, null, null, null, null);
        boolean isSaved = false;
        if (retCursor != null && retCursor.moveToFirst()) {
            retCursor.moveToFirst();
            do {
                String currentId = retCursor.getString(1);
                if (currentId.equals(id)) {
                    isSaved = true;
                    break;
                }
            } while (retCursor.moveToNext());
        } else {
            isSaved = false;
        }
        if(retCursor != null){
            retCursor.close();
        }
        return isSaved;
    }

    /**
     * check if the given id already exists in favorite items
     *
     * @param id - the id by which to check
     * @return true if already saved, false otherwise
     */
    public static boolean isAlreadySaved(String id, Context c) {
        Cursor retCursor = c.getContentResolver().query(PlacesContract.SearchResultsEntry.CONTENT_URI, null, null, null, null);
        boolean isSaved = false;
        if (retCursor != null && retCursor.moveToFirst()) {
            retCursor.moveToFirst();
            do {
                String currentId = retCursor.getString(1);
                if (currentId.equals(id)) {
                    if(retCursor.getInt(10) == 10){
                        isSaved = true;
                        break;
                    }
                }
            } while (retCursor.moveToNext());
        } else {
            isSaved = false;
        }
        if(retCursor != null){
            retCursor.close();
        }
        return isSaved;
    }
}
