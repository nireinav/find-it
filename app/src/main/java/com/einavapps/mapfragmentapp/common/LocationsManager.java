package com.einavapps.mapfragmentapp.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.GnssStatus;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.lang.ref.WeakReference;
import java.util.Observable;

import static android.location.GpsStatus.GPS_EVENT_STARTED;
import static android.location.GpsStatus.GPS_EVENT_STOPPED;

/*
 *this singleton class is designed to manage all location functionality in the app
 *
 */
public class LocationsManager extends Observable implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    //ATTRIBUTES:

    private static final String LAST_SAVED_LAT_KEY = "lastSavedLatitude";
    private static final String LAST_SAVED_LNG_KEY = "lastSavedLongitude";
    private static final long MIN_TIME_FOR_UPDATE_AFTER_SETTINGS_TURNED_ON = 30000;
    private static final int UPDATE_INTERVAL_IN_MILISECONDS = 20000;
    private static final int FASTEST_INTERVAL_IN_MILISECONDS = 10000;
    private static LocationManager locationManager;
    private static LocationsManager mLocationsManager;
    private static Location mCurrentLocation;
    private WeakReference<Activity> weakReferenceActivity;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private long lastTimeStamp;

    //CONSTRUCTORS:
    private LocationsManager(Activity activity) {
        this.weakReferenceActivity = new WeakReference<>(activity);
    }

    //FUNCTIONS:
    public static LocationsManager getInstance(Activity activity) {
        if (mLocationsManager == null) {
            locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
            mLocationsManager = new LocationsManager(activity);
        }
        return mLocationsManager;
    }

    public void buildGoogleApiClient() {
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(weakReferenceActivity.get())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    /**
     * checks and asks for location settings change
     */
    public void askForLocationSettingsChange() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        if (ActivityCompat.checkSelfPermission(weakReferenceActivity.get(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(weakReferenceActivity.get(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }
        getLocationAndNotifyObservers();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * connects the google api client
     */
    public void connectGoogleApiClient() {
        mGoogleApiClient.connect();
    }

    /**
     * disconnects the google api client
     */
    public void disConnectGoogleApiClient() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * gets the last saved location from shared preferences
     *
     * @return location - last saved location
     */
    public Location getLastSavedLocationFromSharedPreferences(Activity activity) {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        Location location = new Location("");
        location.setLatitude(sharedPref.getFloat(LAST_SAVED_LAT_KEY, 51.496715f));
        location.setLongitude(sharedPref.getFloat(LAST_SAVED_LNG_KEY, -0.1763672f));
        return location;
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        notifyObservers();
    }

    private void startLocationUpdates() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL_IN_MILISECONDS);
        if (ActivityCompat.checkSelfPermission(weakReferenceActivity.get(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(weakReferenceActivity.get(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    public void stopLocationUpdates() {
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    public void registerGpsSettingsReceiver() {
        if (ActivityCompat.checkSelfPermission(weakReferenceActivity.get(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locationManager.registerGnssStatusCallback(new GnssStatus.Callback() {
                @Override
                public void onStarted() {
                    super.onStarted();
                    getLocationAndNotifyObservers();
                }
            });
        } else {
            locationManager.addGpsStatusListener(new GpsStatus.Listener() {
                public void onGpsStatusChanged(int event) {
                    switch (event) {
                        case GPS_EVENT_STARTED:
                            getLocationAndNotifyObservers();
                            break;
                        case GPS_EVENT_STOPPED:
                            break;
                    }
                }
            });
        }
    }

    public Location getMyLocation() {
        return mCurrentLocation;
    }

    public void getLocationAndNotifyObservers() {
        if (ActivityCompat.checkSelfPermission(weakReferenceActivity.get(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(weakReferenceActivity.get(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        // start a new location request if lastTimeStamp is 0 (which means this is the first time) or if the
        // minimum amount of time has past.
        if(lastTimeStamp == 0 || (System.currentTimeMillis() - lastTimeStamp > MIN_TIME_FOR_UPDATE_AFTER_SETTINGS_TURNED_ON)) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            notifyObservers();
            //if there was no location found det back lastTime stamp to 0
            lastTimeStamp =  mCurrentLocation == null? 0: System.currentTimeMillis();
        }
    }


    private class ResultCallback implements com.google.android.gms.common.api.ResultCallback<LocationSettingsResult> {


        @Override
        public void onResult(@NonNull LocationSettingsResult result) {
            final Status status = result.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    // All location settings are satisfied. The client can
                    // initialize location requests here.

                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(weakReferenceActivity.get(), 1);
                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    // Location settings are not satisfied. However, we have no way
                    // to fix the settings so we won't show the dialog.
                    break;
            }
        }
    }

    @Override
    public void notifyObservers() {
        setChanged();
        super.notifyObservers(mCurrentLocation);
    }
}
