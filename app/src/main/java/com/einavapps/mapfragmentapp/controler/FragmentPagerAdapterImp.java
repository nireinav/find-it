package com.einavapps.mapfragmentapp.controler;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.common.App;
import com.einavapps.mapfragmentapp.common.Utility;
import com.einavapps.mapfragmentapp.fragments.FavoritesFragment;
import com.einavapps.mapfragmentapp.fragments.MapViewFragment;
import com.einavapps.mapfragmentapp.fragments.SearchFragment;

import java.lang.ref.WeakReference;

public class FragmentPagerAdapterImp extends FragmentPagerAdapter {
    private static final int NORMAL_MODE_FRAGMENT_COUNT = 3;
    private static final int TWO_PANE_MODE_FRAGMENT_COUNT = 2;
    private WeakReference<Activity> activity;
    private FragmentManager fm;

    public FragmentPagerAdapterImp(FragmentManager fm, Activity activity) {
        super(fm);
        this.fm = fm;
        this.activity = new WeakReference<>(activity);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment currentFragment = null;
        switch (position) {
            case 0:
                currentFragment = SearchFragment.newInstance();
                break;
            case 1:
                currentFragment = FavoritesFragment.newInstance();
                break;
            case 2:
                /*
                Since the MapViewFragment is retained, i am checking for the existence of an instance of it
                 using the TAG set by the FragmentPagerAdapter. if no such instance is found i create a new instance
                 */
                currentFragment =  fm.findFragmentByTag(makeFragmentName(2));
                if (currentFragment == null) {
                    currentFragment = MapViewFragment.newInstance();
                }
                break;
        }
        return currentFragment;
    }

    @Override
    public int getCount() {
        return Utility.isTwoPaneMode(activity.get()) ? TWO_PANE_MODE_FRAGMENT_COUNT : NORMAL_MODE_FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String currentTitle = null;
        switch (position) {
            case 0:
                currentTitle = App.get().getString(R.string.first_viewPager_title);
                break;
            case 1:
                currentTitle = App.get().getString(R.string.second_viewPager_title);
                break;
            case 2:
                currentTitle = App.get().getString(R.string.third_viewPager_title);
                break;
        }
        return currentTitle;
    }

    /**
     * creates and returns the Tag of a fragment created by the FragmentPagerAdapter according to a given index
     * @param index the index by which to create the TAG
     * @return the TAG created
     */
    private String makeFragmentName(int index) {
        return "android:switcher:" + R.id.viewpager + ":" + index;
    }
}
