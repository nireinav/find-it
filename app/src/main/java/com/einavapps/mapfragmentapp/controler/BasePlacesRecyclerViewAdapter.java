package com.einavapps.mapfragmentapp.controler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.common.Utility;
import com.einavapps.mapfragmentapp.data.PlacesContract;
import com.einavapps.mapfragmentapp.pojo.Place;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/*
 * this is the root RecyclerView Adapter which the PlacesRecyclerAdapter and the FavoritesRecyclerAdapter extends.
 * it contains all of their shred fun
 */
public class BasePlacesRecyclerViewAdapter extends RecyclerView.Adapter<BasePlacesRecyclerViewAdapter.DataHolder>{
    //ATTRIBUTES:
    protected ArrayList<Place> places;
    private PlacesRecyclerAdapter.ItemClickCallback itemClickCallback;
    Context c;
    private Location mLocation;

    //CONSTRUCTORS:
    public BasePlacesRecyclerViewAdapter(Cursor cur, Context c, Location location, PlacesRecyclerAdapter.ItemClickCallback itemClickCallback) {
        places = Utility.cursorToArrayList(cur);
        mLocation = location;
        this.c = c;
        this.itemClickCallback = itemClickCallback;
    }

    //FUNCTIONS:
    public BasePlacesRecyclerViewAdapter.DataHolder onCreateViewHolder(ViewGroup parent, int viewType){
        return null;
    }

    @Override
    public void onBindViewHolder(final BasePlacesRecyclerViewAdapter.DataHolder holder, final int position) {
        Place currentPlace = places.get(position);
        holder.placeName.setText(currentPlace.getName());
        holder.placeType.setText(currentPlace.getType());
        holder.placeAddress.setText(currentPlace.getAddress());
        String finalDistance = calculateDistance(currentPlace);
        holder.placeDistance.setText(finalDistance);
        int favoriteIcon = currentPlace.isFavorite() ? R.drawable.ic_action_favorite : R.drawable.ic_action_favorite_border;
        holder.favoriteIcon.setImageResource(favoriteIcon);
        holder.placeRating.setText(Double.toString(currentPlace.getRating()));
        if (currentPlace.getIconReference() != null) {
            Picasso.with(c).load(places.get(position).getIconReference())
                    .placeholder(R.drawable.place_holder)
                    .into(holder.placeIcon);
        }
    }

    private String calculateDistance(Place currentPlace) {
        String finalDistance;
        if (mLocation != null) {
            double distance = Utility.calculateDistance(mLocation.getLatitude(), mLocation.getLongitude(), currentPlace.getLatitude(), currentPlace.getLongitude());
            finalDistance = Utility.changeDistanceUnits((float) distance, c);
        } else {
            finalDistance = "N/A";
        }
        return finalDistance;
    }

    @Override
    public int getItemCount() {
        return places == null ? 0 : places.size();
    }


    /**
     * swaps the ArrayList of places with the given one
     * @param newPlaces the new ArrayList of places
     */
    public void swapArrayList(ArrayList<Place> newPlaces) {
        places = newPlaces;
        notifyDataSetChanged();
    }


    class DataHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //ATTRIBUTES:
        private TextView placeName;
        private TextView placeType;
        private TextView placeAddress;
        private TextView placeDistance;
        private TextView placeRating;
        private ImageView favoriteIcon;
        private ImageView placeIcon;
        private View container;

        //CONSTRUCTORS:
        DataHolder(View itemView) {
            super(itemView);
            placeName = (TextView) itemView.findViewById(R.id.list_item_place_name);
            placeType = (TextView) itemView.findViewById(R.id.list_item_place_type);
            placeAddress = (TextView) itemView.findViewById(R.id.list_item_place_address);
            placeDistance = (TextView) itemView.findViewById(R.id.list_item_distance);
            placeRating = (TextView) itemView.findViewById(R.id.list_item_rating);
            favoriteIcon = (ImageView) itemView.findViewById(R.id.list_item_is_favorite);
            favoriteIcon.setOnClickListener(this);
            placeIcon = (ImageView) itemView.findViewById(R.id.list_item_place_icon);
            container = itemView.findViewById(R.id.lbl_root_layout);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.lbl_root_layout) {
                itemClickCallback.onItemClick(view, getAdapterPosition());
            } else if (id == R.id.list_item_is_favorite) {
                updateFavoritePlace(getAdapterPosition());
            }
        }
    }

    public void setLocation(Location location) {
        mLocation = location;
        notifyDataSetChanged();
    }

    public Place getPlaceAtPosition(int position) {
        Place currentPlace = null;
        if (places != null) {
            currentPlace = places.get(position);
        }
        return currentPlace;
    }

    public interface ItemClickCallback {
        void onItemClick(View v, int p);

    }

    /**
     * updates the isFavorite field in data base for a given place
     * 1 ==> favorite, 0==> not favorite
     * @param p the position of the place to change
     */
    
    private void updateFavoritePlace(int p) {
        String id = getPlaceAtPosition(p).getId();
        boolean isFavorite = getPlaceAtPosition(p).isFavorite();
        additionalFunctionalityOnFavoritePress(p);
        int isF;
        if (!Utility.isAlreadySaved(id, c)) {
            isF = isFavorite?  0 : 1;
            ContentValues values = new ContentValues();
            values.put(PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE, isF);
            c.getContentResolver().update(PlacesContract.SearchResultsEntry.CONTENT_URI, values, PlacesContract.SearchResultsEntry.COLUMN_PLACE_ID + " = ?", new String[]{id});
        }
    }

    protected  void additionalFunctionalityOnFavoritePress(int p){

    }

}
