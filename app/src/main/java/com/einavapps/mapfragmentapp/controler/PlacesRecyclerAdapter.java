package com.einavapps.mapfragmentapp.controler;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.pojo.Place;

public class PlacesRecyclerAdapter extends BasePlacesRecyclerViewAdapter {

    public PlacesRecyclerAdapter(Cursor cur, Context c, Location location, PlacesRecyclerAdapter.ItemClickCallback itemClickCallback) {
        super(cur, c, location, itemClickCallback);
    }

    @Override
    public DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(c).inflate(R.layout.search_results_grid_item, parent, false);
        return new DataHolder(view);
    }

    /**
     * this method is called when clicking the favorite icon.
     * @param p the position of the view clicked
     */
    @Override
    protected void additionalFunctionalityOnFavoritePress(int p) {
        setItemAsFavorite(p);
    }

    /**
     * updates the favorite field in the place at the given position
     * @param position the position in which to update favorite;
     */
    private void setItemAsFavorite(int position) {
        Place currentPlace = places.get(position);
        if (currentPlace.isFavorite()) {
            currentPlace.setFavorite(0);
        } else {
            currentPlace.setFavorite(1);
        }
        notifyItemChanged(position);
    }
}
