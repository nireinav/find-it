package com.einavapps.mapfragmentapp.controler;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.fragments.FavoritesFragment;


public class FavoritesRecyclerAdapter extends BasePlacesRecyclerViewAdapter {
    private FavoriteClickCallback favoriteClickCallback;

    public FavoritesRecyclerAdapter(Cursor cur, Context c, Location location,
                                    PlacesRecyclerAdapter.ItemClickCallback itemClickCallback
                                    , FavoriteClickCallback favoriteClickCallback) {
        super(cur, c, location, itemClickCallback);
        this.favoriteClickCallback = favoriteClickCallback;
    }

    @Override
    public DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(c).inflate(R.layout.favorite_grid_item, parent, false);
        return new DataHolder(view);
    }

    @Override
    protected void additionalFunctionalityOnFavoritePress(int p) {
        removePlaceAtPosition(p);
        if(!isThereFavorites()){
            favoriteClickCallback.showNoFavoritesMessage();
        }
    }

    private void removePlaceAtPosition(int p) {
        places.remove(p);
        notifyItemRemoved(p);
    }

    /**
     * checks if there are favorite places saved
     * @return return whether there are favorite places saved
     */
    private boolean isThereFavorites() {
        for (int i = 0; i < places.size(); i++) {
            if (places.get(i).isFavorite()) {
                return true;
            }
        }
        return false;
    }

    public interface FavoriteClickCallback{
        void showNoFavoritesMessage();
    }
}
