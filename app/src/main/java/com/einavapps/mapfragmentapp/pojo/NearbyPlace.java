package com.einavapps.mapfragmentapp.pojo;

import java.util.List;

/**
 * Created by owner on 2/8/2017.
 */

public class NearbyPlace  {

    Geometry geometry;
    String place_id;
    String name;
    String vicinity;
    double rating;
    IsOpen opening_hours;
    Photo[] photos;
    List<String> types;
    private String apiKey = "AIzaSyCgKsjYNr1jQWL2rPhzK7Fr6uZW-D8k6IU";//AIzaSyCjKW2S0NRFtxB6H8hPSnSxOHBldyf8tFg";//AIzaSyCgKsjYNr1jQWL2rPhzK7Fr6uZW-D8k6IU";
    private String baseIconReferance = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=800&photoreference=";

    public Geometry getGeometry() {
        return geometry;
    }

    public String getPlace_id() {
        return place_id;
    }

    public String getName() {
        return name;
    }

    public String getVicinity() {
        return vicinity;
    }

    public double getRating() {
        return rating;
    }

    public int getOpening_hours() {
        if(opening_hours != null && opening_hours.isOpen()){
            return 1;
        }else{
            return 0;
        }
    }

    public String getPhotos() {
        String iconReference = null;
        if(photos != null){
            iconReference =  baseIconReferance + photos[0].getPhoto_reference() + "&key=" + apiKey;
        }
        return iconReference;
    }

    public String getTypes() {
        String type = "";
        for(String s: types){
            type += s + ", ";
        }
        return type.substring(0,type.length()-2);
    }

    public double getLatitude(){
        return geometry.getLocation().getLat();
    }

    public double getLongitude(){
        return geometry.getLocation().getLng();
    }
}

//opening hours:
class IsOpen{
    boolean open_now;
    public boolean isOpen(){
        return open_now;
    }
}

//photos:
class Photo{
    String photo_reference;

    public String getPhoto_reference(){
        return photo_reference;
    }
}

//location:
class Geometry{
    Location location;
    public Location getLocation() {
        return location;
    }
}

class Location{
    double lat;
    double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
