package com.einavapps.mapfragmentapp.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Place implements Parcelable {

    //ATTRIBUTES:
    private String id;
    private String name;
    private String type;
    private String address;
    private String distance;
    private String iconReference;
    private int open;
    private double rating;
    private double longitude;
    private double latitude;
    private int isFavorite;
    private int isHistory;
    private static final int IS_FAVORITE_YES = 1;
    protected Place(Parcel in) {
        id = in.readString();
        name = in.readString();
        type = in.readString();
        address = in.readString();
        distance = in.readString();
        iconReference = in.readString();
        open = in.readInt();
        rating = in.readDouble();
        longitude = in.readDouble();
        latitude = in.readDouble();
        isFavorite = in.readInt();
        isHistory = in.readInt();
    }


    //CONSTRUCTORS:
    public Place(){

    }

    public Place(String name, double longitude, double latitude) {
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Place(String name, String type, String address, String iconReference) {
        this.address = address;
        this.name = name;
        this.type = type;
        this.iconReference = iconReference;
    }

    public Place(String id, String name, String type, String address, String iconReference,
                 int isOpen, double rating, double latitude, double longitude, int isFavorite, int isHistory) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.address = address;
        this.iconReference = iconReference;
        open = isOpen;
        this.rating = rating;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isFavorite = isFavorite;
        this.isHistory = isHistory;
    }

    //FUNCTIONS:

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId(){
        return id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        latitude = latitude;
    }

    public String getIconReference() {

        return iconReference;
    }

    public String getType() {
        return type;
    }

    public String getAddress() {
        return address;
    }

    public String getDistance() {
        return distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean isFavorite() {
        return isFavorite == IS_FAVORITE_YES;
    }

    public void setFavorite(int isFavorite) {
        this.isFavorite = isFavorite;
    }

    public int isHistory() {
        return isHistory;
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    public int getOpen() {
        return open;
    }

    public double getRating() {
        return rating;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(type);
        parcel.writeString(address);
        parcel.writeString(distance);
        parcel.writeString(iconReference);
        parcel.writeInt(open);
        parcel.writeDouble(rating);
        parcel.writeDouble(longitude);
        parcel.writeDouble(latitude);
        parcel.writeInt(isFavorite);
        parcel.writeInt(isHistory);
    }
}
