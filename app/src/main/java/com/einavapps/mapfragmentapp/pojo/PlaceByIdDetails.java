package com.einavapps.mapfragmentapp.pojo;

import java.util.ArrayList;
import java.util.List;



class PlaceByIdDetails {
    private String name;
    private String formatted_address;
    private String international_phone_number;
    private String website;
    private Geometry geometry;
    private List<String> types;
    private Photo[] photos;
    private double rating;
    private IsOpen opening_hours;

    public double getLatitude(){
        return geometry.getLocation().getLat();
    }

    public double getLongitude(){
        return geometry.getLocation().getLng();
    }

    public IsOpen getOpening_hours() {
        return opening_hours;
    }

    public String getName() {
        return name;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public String getInternational_phone_number() {
        return international_phone_number;
    }

    public String getWebsite() {
        return website;
    }

    public double getRating() {
        return rating;
    }

    public List<String> getTypes() {
        return types;
    }

    public ArrayList<String> getPhotos() {
        ArrayList<String> photoReferences = new ArrayList<>();
        for(int i =0; i < photos.length; i++){
            photoReferences.add(photos[i].getPhoto_reference());
        }
        return photoReferences;
    }
}
