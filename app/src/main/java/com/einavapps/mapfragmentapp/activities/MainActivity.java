package com.einavapps.mapfragmentapp.activities;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.View;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.crashlytics.android.Crashlytics;
import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.animations.FadePageTransformer;
import com.einavapps.mapfragmentapp.common.LocationsManager;
import com.einavapps.mapfragmentapp.common.Utility;
import com.einavapps.mapfragmentapp.controler.FragmentPagerAdapterImp;
import com.einavapps.mapfragmentapp.fragments.MapViewFragment;
import com.einavapps.mapfragmentapp.fragments.PlaceDetailFragment;
import com.einavapps.mapfragmentapp.fragments.SearchFragment;
import com.einavapps.mapfragmentapp.intefaces.OnPOIClick;
import com.einavapps.mapfragmentapp.intefaces.SetSearchFragment;
import com.einavapps.mapfragmentapp.pojo.Place;

import io.fabric.sdk.android.Fabric;

import static android.view.View.LAYER_TYPE_NONE;

public class MainActivity extends AppCompatActivity implements SetSearchFragment, SearchFragment.OnItemClick, OnPOIClick {

    //ATTRIBUTES:
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 2;
    private static final String LOCATION_KEY = "currentLocation";
    private AHBottomNavigation mBottomBar;
    private Snackbar noConnectionSnackbar;
    private CoordinatorLayout coordinatorLayout;
    private ViewPager mViewPager;
    private boolean isTwoPane;
    private PowerConnectionReceiver pcReceiver;
    private NetworkChangeReceiver ncReceiver;

    //FUNCTIONS:
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        LocationsManager.getInstance(this).buildGoogleApiClient();
        LocationsManager.getInstance(this).askForLocationSettingsChange();
        isTwoPane = Utility.isTwoPaneMode(this);
        findAndInitViewPagerAndAdapter();
        findAndInitBottomNavigation(savedInstanceState);
        if (isTwoPane) {
            instantiateAndDisplayMapFragment();
        }
        LocationsManager.getInstance(this).registerGpsSettingsReceiver();
    }

    private void findAndInitViewPagerAndAdapter() {
        FragmentPagerAdapterImp mViewPagerAdapter = new FragmentPagerAdapterImp(getFragmentManager(), this);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setPageTransformer(false, new FadePageTransformer(), LAYER_TYPE_NONE);
        mViewPager.setCurrentItem(0);
    }

    private void instantiateAndDisplayMapFragment() {
        MapViewFragment map = initMapFragment();
        replaceMainFragment(map, R.id.two_pane_map_fragment_container);
    }

    private String makeFragmentName(int index) {
        return "android:switcher:" + R.id.viewpager + ":" + index;
    }

    @Override
    protected void onStart() {
        LocationsManager.getInstance(this).connectGoogleApiClient();
        super.onStart();
        checkForLocationPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isTwoPane) {
            coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main_two_pane);
        } else {
            coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main);
        }
        checkForNetworkConnectivity();
        registerChargingReceiver();
        registerNetworkChangeReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Location currentLocation = LocationsManager.getInstance(this).getMyLocation();
        if (currentLocation != null) {
            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putFloat("lastSavedLatitude", (float) currentLocation.getLatitude());
            editor.putFloat("lastSavedLongitude", (float) currentLocation.getLongitude());
            editor.apply();
        }
        unregisterReceiver(pcReceiver);
        unregisterReceiver(ncReceiver);
    }

    @Override
    protected void onStop() {
        LocationsManager.getInstance(this).disConnectGoogleApiClient();
        LocationsManager.getInstance(this).stopLocationUpdates();
        super.onStop();
    }

    private void checkForNetworkConnectivity() {
        if (!Utility.isOnline(this)) {
            showNoConnectionSnackBar();
        }
    }

    private void checkForLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION);
        }
    }

    /**
     * finds and sets the attributes of the bottom navigation bar
     */
    private void findAndInitBottomNavigation(Bundle savedInstanceState) {
        mBottomBar = (AHBottomNavigation) findViewById(R.id.bottom_navigation_bar);
        // Create items
        AHBottomNavigationItem searchItem = new AHBottomNavigationItem(R.string.text_search, R.drawable.ic_search_white_24dp, R.color.colorPrimary);
        AHBottomNavigationItem favoritesItem = new AHBottomNavigationItem(R.string.text_favorites, R.drawable.ic_favorite_border_white_24dp, R.color.colorPrimary);
        // Add items
        mBottomBar.addItem(searchItem);
        mBottomBar.addItem(favoritesItem);
        if (!isTwoPane) {
            AHBottomNavigationItem nearMeItem = new AHBottomNavigationItem(R.string.text_nearby, R.drawable.ic_location_on_white_24dp, R.color.colorPrimary);
            mBottomBar.addItem(nearMeItem);
        } else {
            mBottomBar.setBehaviorTranslationEnabled(false);
        }
        // Set background color
        mBottomBar.setDefaultBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        mBottomBar.setColored(true);
        mBottomBar.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (!isTwoPane) {
                    switch (position) {
                        case 0:
                        case 1:
                            mBottomBar.setAlpha(1f);
                            break;
                        case 2:
                            mBottomBar.setAlpha(0.7f);
                            break;
                    }
                }
                mViewPager.setCurrentItem(position);
                return true;
            }
        });

        PlaceDetailFragment fragment = (PlaceDetailFragment) getFragmentManager().findFragmentById(R.id.main_container);
        if (fragment != null) {
            mBottomBar.hideBottomNavigation(false);
        }

        if (savedInstanceState != null && savedInstanceState.containsKey(getString(R.string.view_pager_position))) {
            int position = savedInstanceState.getInt(getString(R.string.view_pager_position));
            if (position == 2) {
                mBottomBar.setAlpha(0.7f);
            }
        }
    }

    /**
     * Replaces the fragment in the main container with a given fragment
     *
     * @param fragment - the fragment to add
     */
    private void replaceMainFragment(Fragment fragment, int containerId) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (fragment instanceof PlaceDetailFragment) {
            transaction.addToBackStack(getString(R.string.detail_fragment_key));
        }
        transaction.replace(containerId, fragment, "android:switcher:" + R.id.viewpager + ":" + 2);
        transaction.commit();
    }

    /**
     * creates and defines the map fragment
     *
     * @return mapFragment - the created map fragment
     */
    private MapViewFragment initMapFragment() {
        MapViewFragment mapFragment;
        mapFragment = (MapViewFragment) getFragmentManager().findFragmentByTag(makeFragmentName(2));
        if (mapFragment == null) {
            mapFragment = MapViewFragment.newInstance();
        }
        return mapFragment;
    }

    /**
     * registers the action battery change receiver
     */
    private void registerNetworkChangeReceiver() {
        IntentFilter ifilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        ncReceiver = new NetworkChangeReceiver();
        registerReceiver(ncReceiver, ifilter);
    }

    /**
     * registers the action battary change receiver
     */
    private void registerChargingReceiver() {
        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction(Intent.ACTION_POWER_CONNECTED);
        ifilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        pcReceiver = new PowerConnectionReceiver();
        registerReceiver(pcReceiver, ifilter);
    }

    //a function called from map fragment
    @Override
    public void onPOIClick() {
        mBottomBar.hideBottomNavigation(true);
    }

    public class PowerConnectionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
                showSnackBar(getString(R.string.charging_card), true);
            } else {
                (intent.getAction()).equals(Intent.ACTION_POWER_DISCONNECTED);
                showSnackBar(getString(R.string.done_charging_card), true);
            }
        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Utility.isOnline(context)) {
                if (noConnectionSnackbar != null && noConnectionSnackbar.isShown()) {
                    noConnectionSnackbar.dismiss();
                }
            } else {
                if (noConnectionSnackbar == null || !noConnectionSnackbar.isShown()) {
                    showNoConnectionSnackBar();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                LocationsManager.getInstance(this).getLocationAndNotifyObservers();
            } else {
                Snackbar.make(findViewById(android.R.id.content), R.string.u_need_to_allow_use_of_location, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    /**
     * shows the charging snackbar
     *
     * @param text - the text written in the snackbar
     */
    private void showSnackBar(String text, boolean showChargingImage) {
        Snackbar snackbar;
        if (showChargingImage) {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            builder.append(" ").append(" ");
            builder.setSpan(new ImageSpan(MainActivity.this, R.drawable.ic_action_battery_charging_full), builder.length() - 1, builder.length(), 0);
            builder.append(" " + text);
            snackbar = Snackbar.make(coordinatorLayout, builder, Snackbar.LENGTH_LONG);
        } else {
            snackbar = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG);
        }
        snackbar.getView().setBackgroundColor(Color.parseColor("#8A000000"));
        snackbar.show();
    }

    private void showNoConnectionSnackBar() {
        noConnectionSnackbar = Snackbar.make(coordinatorLayout, R.string.no_connection, Snackbar.LENGTH_LONG);
        noConnectionSnackbar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkForNetworkConnectivity();
            }
        });
        noConnectionSnackbar.setActionTextColor(ContextCompat.getColor(this, R.color.nearMeSecondaryColor));
        noConnectionSnackbar.show();
    }

    //methods activated from search fragment
    @Override
    public void onItemSelected(double latitude, double longitude, Place place, View v) {
        if (isTwoPane) {
            MapViewFragment mapFragment = (MapViewFragment) getFragmentManager().findFragmentById(R.id.two_pane_map_fragment_container);
            mapFragment.selectPlaceInTwoPaneMode(latitude, longitude);
        }
        mBottomBar.hideBottomNavigation(true);
    }

    /**
     * changes the current favorite fragment to a search fragment
     */
    @Override
    public void showSearchFragment() {
        mViewPager.setCurrentItem(0);
        mBottomBar.setCurrentItem(0);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int currentPosition = mViewPager.getCurrentItem();
        outState.putInt(getString(R.string.view_pager_position), currentPosition);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            mBottomBar.setVisibility(View.VISIBLE);
            mBottomBar.restoreBottomNavigation(true);
        } else {
            finish();
        }
    }
}
