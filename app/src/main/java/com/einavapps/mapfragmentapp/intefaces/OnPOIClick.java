package com.einavapps.mapfragmentapp.intefaces;


public interface OnPOIClick {
    /**
     * for when a place has been selected.
     */
    void onPOIClick();
}