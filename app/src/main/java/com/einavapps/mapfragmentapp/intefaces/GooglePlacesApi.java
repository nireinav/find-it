package com.einavapps.mapfragmentapp.intefaces;

import com.einavapps.mapfragmentapp.pojo.NearbyPlaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface GooglePlacesApi {

    //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362
    // &radius=500&type=restaurant&keyword=cruise&key=YOUR_API_KEY

    @GET("/maps/api/place/nearbysearch/json")
    Call<NearbyPlaces> getNearbyPlaces(@Query("location") String location, @Query("radius") String radius, @Query("key") String apiKey);

    //https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJN1t_tDeuEmsRUsoyG83frY4&key=YOUR_API_KEY
    @GET("/maps/api/place/details/json")
    Call<ResponseBody> getPlaceById(@Query("placeid") String placeId, @Query("key") String apiKey);
}
