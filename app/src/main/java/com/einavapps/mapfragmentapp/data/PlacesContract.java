
/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.einavapps.mapfragmentapp.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Defines table and column names for the weather database.
 */
public class PlacesContract {

    public static final String[] PLACES_COLUMNS = {
            SearchResultsEntry._ID,
            SearchResultsEntry.COLUMN_PLACE_ID,
            SearchResultsEntry.COLUMN_PLACE_NAME,
            SearchResultsEntry.COLUMN_PLACE_TYPE,
            SearchResultsEntry.COLUMN_PLACE_ADDRESS,
            SearchResultsEntry.COLUMN_PLACE_ICON_REFERENCE,
            SearchResultsEntry.COLUMN_PLACE_IS_OPEN,
            SearchResultsEntry.COLUMN_RATING,
            SearchResultsEntry.COLUMN_COORD_LAT,
            SearchResultsEntry.COLUMN_COORD_LONG,
            SearchResultsEntry.COLUMN_IS_FAVORITE,
            SearchResultsEntry.COLUMN_IS_HISTORY
    };

    // The "Content authority" is a name for the entire content provider, similar to the
    // relationship between a domain name and its website.  A convenient string to use for the
    // content authority is the package name for the app, which is guaranteed to be unique on the
    // device.
    static final String CONTENT_AUTHORITY = "com.einavapps.placeSearch";
//    public static final String CONTENT_AUTHORITY = "com.example.android.sunshine.app";
    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths (appended to base content URI for possible URI's)
    // For instance, content://com.example.android.sunshine.app/weather/ is a valid path for
    // looking at weather data. content://com.example.android.sunshine.app/givemeroot/ will fail,
    // as the ContentProvider hasn't been given any information on what to do with "givemeroot".
    // At least, let's hope not.  Don't be that dev, reader.  Don't be that dev.
    static final String PATH_TEXT_SEARCH = "searchResults";

    /*
        Inner class that defines the table contents of the location table
        Students: This is where you will add the strings.  (Similar to what has been
        done for WeatherEntry)
     */
    public static final class SearchResultsEntry implements BaseColumns {
        static final String TABLE_NAME = "searchResults";
        public static final String COLUMN_PLACE_ID = "place_id";
        public static final String COLUMN_PLACE_NAME = "place_name";
        public static final String COLUMN_PLACE_TYPE = "place_type";
        public static final String COLUMN_PLACE_ADDRESS = "place_address";
        public static final String COLUMN_PLACE_ICON_REFERENCE = "place_iconReference";
        //Is the place open stored as an int -  1 ==> open, 0 ==> closed:
        public static final String COLUMN_PLACE_IS_OPEN = "place_is_open";
        public static final String COLUMN_RATING = "rating";
        //Coordinates stored as doubles
        public static final String COLUMN_COORD_LAT = "coord_lat";
        public static final String COLUMN_COORD_LONG = "coord_long";
        public static final String COLUMN_IS_FAVORITE= "is_favorite";
        public static final String COLUMN_IS_HISTORY= "is_history";



        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TEXT_SEARCH).build();

        public static Uri buildSearchResultsUri() {
            return CONTENT_URI;
        }

        public static Uri buildFavoritesUri() {
            return CONTENT_URI.buildUpon().appendPath("favorites").build();
        }


        static Uri buildSearchResultsUris(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }
}