package com.einavapps.mapfragmentapp.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class SearchResultsProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    static final int PLACES = 100;
    static final int FAVORITE_PLACES= 101;
    static final int PLACES_HISTORY= 102;

    private static PlacesDbHelper mOpenHelper;


    @Override
    public boolean onCreate() {
        mOpenHelper = PlacesDbHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            case PLACES:
                selectionArgs = new String[]{"1"};
                selection = sHistorySelection;
                retCursor = getPlaces(projection, selection, selectionArgs, sortOrder);
                break;
            case FAVORITE_PLACES:
                selectionArgs = new String[]{"1"};
                selection = sFavoriteSelection;
                retCursor = getPlaces(projection, selection, selectionArgs, sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        Uri returnUri;

        long _id = db.insert(PlacesContract.SearchResultsEntry.TABLE_NAME, null, values);
        if (_id > 0)
            returnUri = PlacesContract.SearchResultsEntry.buildSearchResultsUris(_id);
        else
            throw new android.database.SQLException("Failed to insert row into " + uri);
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        return db.delete(PlacesContract.SearchResultsEntry.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int rowsUpdated;

        rowsUpdated = db.update(PlacesContract.SearchResultsEntry.TABLE_NAME, values, selection, selectionArgs);
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        int returnCount = 0;
        try {
            delete(uri, PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE + " = ?", new String[] {"0"});
            for (ContentValues value : values) {
                long _id = db.insert(PlacesContract.SearchResultsEntry.TABLE_NAME, null, value);
                if (_id != -1) {
                    returnCount++;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnCount;
    }

    //searchResults.IS_FAVORITE = ?
    private static final String sFavoriteSelection =
            PlacesContract.SearchResultsEntry.TABLE_NAME +
                    "." + PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE + " = ? ";

    //searchResults.IS_HISTORY = ?
    private static final String sHistorySelection =
            PlacesContract.SearchResultsEntry.TABLE_NAME +
                    "." + PlacesContract.SearchResultsEntry.COLUMN_IS_HISTORY+ " = ? ";

    private Cursor getPlaces(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return mOpenHelper.getReadableDatabase().query(
                PlacesContract.SearchResultsEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }

    static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        // 2) Use the addURI function to match each of the types.  Use the constants from
        // WeatherContract to help define the types to the UriMatcher.
        final String authority = PlacesContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        matcher.addURI(authority, PlacesContract.PATH_TEXT_SEARCH, PLACES);
        matcher.addURI(authority, PlacesContract.PATH_TEXT_SEARCH + "/*", FAVORITE_PLACES);
        matcher.addURI(authority, PlacesContract.PATH_TEXT_SEARCH + "/#/*", PLACES_HISTORY);

        return matcher;
    }

}
