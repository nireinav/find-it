package com.einavapps.mapfragmentapp.data;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Manages a local database for weather data.
 */
class PlacesDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    private static PlacesDbHelper dBHelperFirstInstance = null;

    private static final String DATABASE_NAME = "places.db";

    private PlacesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    static PlacesDbHelper getInstance(Context context) {
        if (dBHelperFirstInstance == null) {
            dBHelperFirstInstance = new PlacesDbHelper(context);
        }
        return dBHelperFirstInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_PLACES_TABLE = "CREATE TABLE " + PlacesContract.SearchResultsEntry.TABLE_NAME + " (" +

                PlacesContract.SearchResultsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                PlacesContract.SearchResultsEntry.COLUMN_PLACE_ID + " TEXT NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_PLACE_NAME + " TEXT NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_PLACE_TYPE + " TEXT NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_PLACE_ADDRESS + " TEXT NOT NULL," +
                PlacesContract.SearchResultsEntry.COLUMN_PLACE_ICON_REFERENCE + " TEXT NOT NULL," +
                PlacesContract.SearchResultsEntry.COLUMN_PLACE_IS_OPEN + " INT NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_RATING + " REAL NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_COORD_LAT + " REAL NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_COORD_LONG + " REAL NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE + " INT NOT NULL, " +
                PlacesContract.SearchResultsEntry.COLUMN_IS_HISTORY + " INT NOT NULL);";

                sqLiteDatabase.execSQL(SQL_CREATE_PLACES_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your application.
        // If you want to update the schema without wiping data, commenting out the next 2 lines
        // should be your top priority before modifying this method.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PlacesContract.SearchResultsEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
