package com.einavapps.mapfragmentapp.fragments;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.activities.SettingsActivity;
import com.einavapps.mapfragmentapp.common.LocationsManager;
import com.einavapps.mapfragmentapp.common.RetrivePlacesIntentService;
import com.einavapps.mapfragmentapp.common.Utility;
import com.einavapps.mapfragmentapp.controler.PlacesRecyclerAdapter;
import com.einavapps.mapfragmentapp.data.PlacesContract;
import com.einavapps.mapfragmentapp.pojo.Place;

import java.util.Observable;
import java.util.Observer;

public class SearchFragment extends PlacesBaseListFragment implements Observer {

    private FloatingSearchView searchView;
    private boolean isFirstInitializationOfRecyclerView = true;
    private boolean mIsUseLocation = false;
    private FrameLayout progressBarLayout;
    private RelativeLayout mNothingInDataBaseMessage;
    private GridLayoutManager layoutManager;
    private int mPosition = 0;

    public SearchFragment() {
    }

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void additionalMethodsInOnCreateView(View v, Bundle savedInstanceState) {
        progressBarLayout = (FrameLayout) v.findViewById(R.id.progress_bar_layout);
        mNothingInDataBaseMessage = (RelativeLayout) v.findViewById(R.id.first_entrance_message);
        if (savedInstanceState != null && savedInstanceState.containsKey(ARG_CURRENT_POSITION)) {
            mPosition = savedInstanceState.getInt(ARG_CURRENT_POSITION);
        }
        findAndInitSearchView(v);
        findAndInitLocationSwitch(v);
    }

    @Override
    public RecyclerView findRecyclerView(View v) {
        return (RecyclerView) v.findViewById(R.id.search_recyclerview);
    }

    @Override
    public View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    private void findAndInitLocationSwitch(final View v) {
        final Switch locationSwitch = (Switch) v.findViewById(R.id.location_switch);
        final TextView useLocationTextView = (TextView) v.findViewById(R.id.use_location_text);
        locationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsUseLocation = true;
                    useLocationTextView.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
                    useLocationTextView.setText(R.string.use_location);
                    useLocationTextView.setTypeface(null, Typeface.BOLD);
                    if (mLocation == null) {
                        locationSwitch.setChecked(false);
                        switchLocationSwitchOff(useLocationTextView);
                        Snackbar snackbar = Snackbar.make(v, R.string.no_location_available, Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                } else {
                    switchLocationSwitchOff(useLocationTextView);
                }
            }
        });
    }

    private void switchLocationSwitchOff(TextView useLocationTextView) {
        mIsUseLocation = false;
        useLocationTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorBackground));
        useLocationTextView.setText(R.string.dont_use_location);
        useLocationTextView.setTypeface(null, Typeface.NORMAL);
    }

    private void findAndInitSearchView(View v) {
        searchView = (FloatingSearchView) v.findViewById(R.id.floating_search_view);
        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                progressBarLayout.setVisibility(View.VISIBLE);
                String tempPlaceQuery = currentQuery.trim();
                String placeQuery = Uri.encode(tempPlaceQuery);
                updateResults(placeQuery, mIsUseLocation);
                updateItemsAsNonHistory();
                searchView.setSearchText("");
            }
        });
        searchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_settings) {
                    Intent intent = new Intent(getActivity(), SettingsActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void updateResults(String placeQuery, boolean isUseLocation) {
        Intent intent = new Intent(getActivity(), RetrivePlacesIntentService.class);
        intent.putExtra("query", placeQuery);
        intent.putExtra("is_use_location", isUseLocation);
        if (mLocation != null) {
            String latLng = Double.toString(mLocation.getLatitude()) + "," + Double.toString(mLocation.getLongitude());
            intent.putExtra("latlng", latLng);
        }
        getActivity().startService(intent);
    }

    @Override
    public Uri buildUri() {
        return PlacesContract.SearchResultsEntry.buildSearchResultsUri();
    }

    @Override
    public void onCursorReceived(Cursor data) {
        int visibilty = mNothingInDataBaseMessage.getVisibility();
        if(!data.moveToFirst() && visibilty == View.GONE){
            mNothingInDataBaseMessage.setVisibility(View.VISIBLE);
        }else if(visibilty == View.VISIBLE){
            mNothingInDataBaseMessage.setVisibility(View.GONE);
        }
        if (isFirstInitializationOfRecyclerView) {
            initRecyclerView(data);
            isFirstInitializationOfRecyclerView = false;
        } else {
            mAdapter.swapArrayList(Utility.cursorToArrayList(data));
        }
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void additionalMethodsInOnLoaderReset() {
        progressBarLayout.setVisibility(View.GONE);
    }

    private void initRecyclerView(Cursor cursor) {
        recView.setHasFixedSize(true);
        layoutManager = (new GridLayoutManager(getActivity(), 1));
        recView.setLayoutManager(layoutManager);
        mAdapter = new PlacesRecyclerAdapter(cursor, getActivity(), mLocation, this);
        recView.setAdapter(mAdapter);
        layoutManager.scrollToPositionWithOffset(mPosition, 20);
    }

    public void updateItemsAsNonHistory() {
        ContentValues values = new ContentValues();
        values.put(PlacesContract.SearchResultsEntry.COLUMN_IS_HISTORY, 0);
        getActivity().getContentResolver().update(PlacesContract.SearchResultsEntry.CONTENT_URI, values,
                PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE + " = ?", new String[]{"1"});
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Location && mAdapter != null) {
            mLocation = (Location)arg;
            mAdapter.setLocation(mLocation);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        LocationsManager.getInstance(getActivity()).deleteObserver(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        LocationsManager.getInstance(getActivity()).addObserver(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(layoutManager != null) {
            int position = layoutManager.findFirstCompletelyVisibleItemPosition();
            if (position == -1) {
                position = layoutManager.findFirstVisibleItemPosition();
            }
            outState.putInt(ARG_CURRENT_POSITION, position);
        }
        super.onSaveInstanceState(outState);
    }

    /**
     * A callback interface that all activities containing this fragment will
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface OnItemClick {
        /**
         * DetailFragmentCallback for when an item has been selected.
         */
        void onItemSelected(double latitude, double longitude, Place place, View v);
    }
}