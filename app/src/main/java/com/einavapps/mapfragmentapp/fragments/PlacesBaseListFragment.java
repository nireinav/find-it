package com.einavapps.mapfragmentapp.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.controler.BasePlacesRecyclerViewAdapter;
import com.einavapps.mapfragmentapp.controler.PlacesRecyclerAdapter;
import com.einavapps.mapfragmentapp.data.PlacesContract;
import com.einavapps.mapfragmentapp.pojo.Place;

/*
 *this class is the root fragment the search and favorite fragments extends.
 * it contains all of their shared functionality
 */
public abstract class PlacesBaseListFragment extends Fragment implements PlacesRecyclerAdapter.ItemClickCallback, LoaderManager.LoaderCallbacks<Cursor> {

    RecyclerView recView;
    BasePlacesRecyclerViewAdapter mAdapter;

    protected Location mLocation;
    boolean isFirstInitializationOfRecyclerView = true;
    boolean dispatching;
    private static final int PLACES_LOADER = 0;
    static final String ARG_CURRENT_POSITION = "current position";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflateView(inflater, container);
        recView = findRecyclerView(v);
        additionalMethodsInOnCreateView(v, savedInstanceState);
        return v;
    }

    /**
     * a method to be overridden by any child fragment
     * @param v the fragment view to inflate
     */
    public abstract void additionalMethodsInOnCreateView(View v, Bundle savedInstanceState);

    /**
     * a method to be overridden by any child fragment and creates the RecyclerView that displays the places
     * @param v the fragment view to inflate
     * @return the RecyclerView created
     */
    public abstract RecyclerView findRecyclerView(View v);

    /**
     * a method to be overridden by any child fragment and inflates the Fragment's view
     * @param inflater  the inflater by which to inflate the fragment
     * @param container the container to inflate the fragment to
     * @return the inflated view
     */
    public abstract View inflateView(LayoutInflater inflater, ViewGroup container);


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(PLACES_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onItemClick(View v, int p) {
        Place currentPlace = mAdapter.getPlaceAtPosition(p);
        ((SearchFragment.OnItemClick) getActivity()).onItemSelected(mAdapter.getPlaceAtPosition(p).getLatitude(),
                mAdapter.getPlaceAtPosition(p).getLongitude(), currentPlace, v);
        dispatchDetailFragment(v, p);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri resultsUri = buildUri();
        return new CursorLoader(getActivity(),
                resultsUri,
                PlacesContract.PLACES_COLUMNS,
                null,
                null,
                null);
    }

    /**
     * creates the Uri by which the content provider is queried
     * @return the built Uri
     */
    public abstract Uri buildUri();

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        onCursorReceived(data);
    }

    /**
     * a method to be overridden by any child fragment, describes action when cursor received from content provider
     * @param data the cursor received
     */
    public abstract void onCursorReceived(Cursor data);

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (mAdapter != null) {
            mAdapter.swapArrayList(null);
        }
        additionalMethodsInOnLoaderReset();
    }

    /**
     * a method to be overridden by any child fragment, describes action when cursor reset
     */
    public abstract void additionalMethodsInOnLoaderReset();

    /**
     * a method that creates and displays a new placeDetailFragment
     * @param v the view clicked
     * @param p the position of the clicked item in mAdapter
     */
    private void dispatchDetailFragment(View v, int p) {
        if (!dispatching) {
            Bundle bundle;
            PlaceDetailFragment fragment = (PlaceDetailFragment) getActivity().getFragmentManager().findFragmentByTag("vvv");
            if (fragment == null) {
                fragment = new PlaceDetailFragment();
                bundle = new Bundle();
                fragment.setArguments(bundle);
            } else {
                bundle = fragment.getArguments();
            }
            setDataInBundle(bundle, p);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Inflate transitions to apply
                Transition changeTransform = TransitionInflater.from(getActivity()).
                        inflateTransition(R.transition.change_image_transform);
                Transition explodeTransform = TransitionInflater.from(getActivity()).
                        inflateTransition(android.R.transition.explode);
                // Setup enter transition on second fragment
                fragment.setSharedElementEnterTransition(changeTransform);
                fragment.setEnterTransition(explodeTransform);

                // Find the shared element (in FragmentMain)
                ImageView placePhoto = (ImageView) v.findViewById(R.id.list_item_place_icon);
            }
            // Add second fragment by replacing first
            FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction()
                    .replace(R.id.main_container, fragment, "vvv")
                    .addToBackStack("transaction");
            ft.commit();
            dispatching = true;
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dispatching = false;
                }
            }, 1000);
        }
    }

    /**
     * loads data of a specific place inside given bundle
     * @param bundle the bundle to load data in
     * @param p      place position in mAdapter
     */
    private void setDataInBundle(Bundle bundle, int p) {
        bundle.putString("place_id", mAdapter.getPlaceAtPosition(p).getId());
        bundle.putString("place_name", mAdapter.getPlaceAtPosition(p).getName());
        bundle.putString("place_type", mAdapter.getPlaceAtPosition(p).getType());
        bundle.putString("place_address", mAdapter.getPlaceAtPosition(p).getAddress());
        bundle.putDouble("place_rating", mAdapter.getPlaceAtPosition(p).getRating());
        bundle.putString("place_icon_url", mAdapter.getPlaceAtPosition(p).getIconReference());
        bundle.putInt("place_is_open", mAdapter.getPlaceAtPosition(p).getOpen());
        bundle.putBoolean("place_is_favorite", mAdapter.getPlaceAtPosition(p).isFavorite());
        bundle.putDouble("place_lat", mAdapter.getPlaceAtPosition(p).getLatitude());
        bundle.putDouble("place_lng", mAdapter.getPlaceAtPosition(p).getLongitude());
        bundle.putParcelable("current_location", mLocation);
    }
}
