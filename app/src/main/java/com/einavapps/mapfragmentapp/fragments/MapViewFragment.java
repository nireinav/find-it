package com.einavapps.mapfragmentapp.fragments;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.common.LocationsManager;
import com.einavapps.mapfragmentapp.common.Utility;
import com.einavapps.mapfragmentapp.intefaces.GooglePlacesApi;
import com.einavapps.mapfragmentapp.intefaces.OnPOIClick;
import com.einavapps.mapfragmentapp.pojo.NearbyPlace;
import com.einavapps.mapfragmentapp.pojo.NearbyPlaces;
import com.einavapps.mapfragmentapp.view.WaitingIndicator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapViewFragment extends Fragment implements OnMapReadyCallback, Callback<NearbyPlaces>, Observer {

    GoogleMap myMap;
    MapView mMapView;
    static Location mCurrentLocation;
    private ArrayList<NearbyPlace> mNearbyPlacesArray;
    private FrameLayout waitingIndicatorCardview;
    private WaitingIndicator waitingIndicator;

    public MapViewFragment() {
    }

    public static MapViewFragment newInstance() {
        return new MapViewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map_view, container, false);
        findAndInitFabs(v);
        waitingIndicatorCardview = (FrameLayout) v.findViewById(R.id.waiting_indicator_cardview);
        waitingIndicator = (WaitingIndicator) v.findViewById(R.id.waiting_indicator);
        MapsInitializer.initialize(this.getActivity());
        mMapView = (MapView) v.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
        LocationsManager.getInstance(getActivity()).addObserver(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
        LocationsManager.getInstance(getActivity()).deleteObserver(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        myMap = googleMap;
        if(mCurrentLocation != null){
            animateCameraToPosition(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 2000);
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        myMap.setMyLocationEnabled(true);
        myMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (mNearbyPlacesArray != null) {
            addNearByLocationsMarkersToMap(mNearbyPlacesArray);
        }
        myMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String placeID = marker.getTag().toString();
                dispatchDetailFragment(placeID);

            }
        });
        myMap.setOnPoiClickListener(new GoogleMap.OnPoiClickListener() {
            @Override
            public void onPoiClick(PointOfInterest pointOfInterest) {
                dispatchDetailFragment(pointOfInterest.placeId);
            }
        });
    }

    /**
     * animates the map camera to the given position
     * @param latitude  - target position's latitude
     * @param longitude - target position's longitude
     * @param duration  - duration of the animation
     */
    private void animateCameraToPosition(double latitude, double longitude, int duration) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))// Sets the center of the map to location user
                .zoom(15)// Sets the zoom
                .bearing(0)// Sets the orientation of the camera to east
                .tilt(0)// Sets the tilt of the camera to 30 degrees
                .build();// Creates a CameraPosition from the builder
        myMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), duration, null);
    }


    /**
     * finds the fab and sets the OnClickListener function.
     */
    private void findAndInitFabs(View v) {
        FloatingActionButton myLocationFab;
        FloatingActionButton mNearMiSearchFab;
        myLocationFab = (FloatingActionButton) v.findViewById(R.id.location_fab);
        mNearMiSearchFab = (FloatingActionButton) v.findViewById(R.id.near_by_search_fab);
        myLocationFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationsManager.getInstance(getActivity()).askForLocationSettingsChange();
                hideProgressbarIfVisible();
                if (mCurrentLocation != null && myMap != null) {
                    animateCameraToPosition(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 2000);
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    }
                    myMap.setMyLocationEnabled(true);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_location_available), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mNearMiSearchFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationsManager.getInstance(getActivity()).askForLocationSettingsChange();
                if (mCurrentLocation != null && myMap != null) {
                    animateCameraToPosition(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 2000);
                    findNearByPlacesIfLocationAvailable();
                }
            }
        });
    }

    /**
     * adds markers to map given an array of places
     *
     * @param nearbyPlaces the array of places to mark
     */
    private void addNearByLocationsMarkersToMap(ArrayList<NearbyPlace> nearbyPlaces) {
        if (myMap != null) {
            myMap.clear();
            for (NearbyPlace place : nearbyPlaces) {
                Marker marker = myMap.addMarker(new MarkerOptions().position(new LatLng(place.getLatitude(), place.getLongitude()))
                        .title(place.getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_location_on)));
                marker.setTag(place.getPlace_id());
            }
        }
    }

    /**
     * finds places near the known location
     */
    private void findNearByPlacesIfLocationAvailable() {
        if (mCurrentLocation != null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            GooglePlacesApi googlePlacesApi = retrofit.create(GooglePlacesApi.class);
            String location = String.valueOf(mCurrentLocation.getLatitude() + "," + String.valueOf(mCurrentLocation.getLongitude()));
            Call<NearbyPlaces> call = googlePlacesApi.getNearbyPlaces(location, "1000", "AIzaSyCgKsjYNr1jQWL2rPhzK7Fr6uZW-D8k6IU");
            call.enqueue(this);
            waitingIndicator.startWaitingAnimation();
            waitingIndicatorCardview.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getActivity(), R.string.no_location_available, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponse(Call<NearbyPlaces> call, Response<NearbyPlaces> response) {
        mNearbyPlacesArray = (ArrayList<NearbyPlace>) response.body().results;
        waitingIndicatorCardview.setVisibility(View.GONE);
        addNearByLocationsMarkersToMap(mNearbyPlacesArray);
    }

    @Override
    public void onFailure(Call<NearbyPlaces> call, Throwable t) {

    }

    /**
     * creates and displays a new detail fragment
     *
     * @param placeId the place id to display in the detail fragment
     */
    private void dispatchDetailFragment(String placeId) {
        int p = findPositionById(placeId);
        if (mNearbyPlacesArray != null && mNearbyPlacesArray.get(p).getVicinity() == null) {
            Toast.makeText(getActivity(), R.string.no_address, Toast.LENGTH_SHORT).show();
        } else {
            PlaceDetailFragment fragment = (PlaceDetailFragment) getFragmentManager().findFragmentByTag(PlaceDetailFragment.class.getName());
            if (fragment == null || Utility.isTwoPaneMode(getActivity())) {
                fragment = new PlaceDetailFragment();
            }
            Bundle bundle = new Bundle();
            bundle.putString("place_id", mNearbyPlacesArray.get(p).getPlace_id());
            bundle.putString("place_name", mNearbyPlacesArray.get(p).getName());
            bundle.putString("place_type", mNearbyPlacesArray.get(p).getTypes());
            bundle.putString("place_address", mNearbyPlacesArray.get(p).getVicinity());
            bundle.putDouble("place_rating", mNearbyPlacesArray.get(p).getRating());
            bundle.putString("place_icon_url", mNearbyPlacesArray.get(p).getPhotos());
            bundle.putInt("place_is_open", mNearbyPlacesArray.get(p).getOpening_hours());
            bundle.putInt("place_is_favorite", 0);
            bundle.putDouble("place_lat", mNearbyPlacesArray.get(p).getLatitude());
            bundle.putDouble("place_lng", mNearbyPlacesArray.get(p).getLongitude());
            bundle.putParcelable("current_location", mCurrentLocation);
            fragment.setArguments(bundle);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Inflate transitions to apply
                Transition changeTransform = TransitionInflater.from(getActivity()).
                        inflateTransition(R.transition.change_image_transform);
                Transition explodeTransform = TransitionInflater.from(getActivity()).
                        inflateTransition(android.R.transition.explode);
                // Setup enter transition on second fragment
                fragment.setSharedElementEnterTransition(changeTransform);
                fragment.setEnterTransition(explodeTransform);
            }
            // Add second fragment by replacing first
            FragmentTransaction ft = getFragmentManager().beginTransaction()
                    .replace(R.id.main_container, fragment, fragment.getClass().getName())
                    .addToBackStack("transaction");
            ft.commit();
            ((OnPOIClick) getActivity()).onPOIClick();
        }
    }

    /**
     * checks the position in the array of a given place id
     *
     * @param placeId the place id whose position needs to be found
     * @return the position found
     */
    private int findPositionById(String placeId) {
        int position = -1;
        if(mNearbyPlacesArray != null) {
            for (int i = 0; i < mNearbyPlacesArray.size(); i++) {
                if (mNearbyPlacesArray.get(i).getPlace_id().equals(placeId)) {
                    position = i;
                    break;
                }
            }
        }
        return position;
    }

    /**
     * hids progressbar if visible
     */
    private void hideProgressbarIfVisible() {
        if (waitingIndicatorCardview != null && waitingIndicatorCardview.getVisibility() == View.VISIBLE)
            waitingIndicatorCardview.setVisibility(View.GONE);
    }

    public void selectPlaceInTwoPaneMode(double latitude, double longitude) {
        animateCameraToPosition(latitude, longitude, 2000);
        myMap.clear();
        myMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_location_green)));
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Location) {
            mCurrentLocation = (Location)arg;
        }
        if(mCurrentLocation != null){
            animateCameraToPosition(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 2000);
        }
    }
}


