package com.einavapps.mapfragmentapp.fragments;


import android.Manifest;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.common.Utility;
import com.einavapps.mapfragmentapp.intefaces.GooglePlacesApi;
import com.einavapps.mapfragmentapp.data.PlacesContract;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceDetailFragment extends Fragment implements Callback<ResponseBody> {
    //ATTRIBUTES:
    private Toolbar toolbar;
    private ImageView placePhoto;
    private TextView placeNameTV;
    private TextView placeTypeTV;
    private TextView placeAddressTV;
    private TextView websiteTV;
    private TextView shareTV;
    private TextView directionsTV;
    private TextView callTV;
    private TextView distanceTV;
    private TextView isOpenTV;
    private TextView ratingTv;
    private String placeID;
    private FloatingActionButton favoriteFab;
    private boolean isAttached;
    private Location mLocation;

    private double lat;
    private double lng;
    private double rating;
    private int isOpen;
    private boolean isFavorite;
    private String types;
    private String website;
    private String name;
    private String phone;
    private String address;
    private String iconUrl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataFromArguments();
        if (placeID != null) {
            findPlacesById();
        }
    }

    /**
     * gets the data from arguments if this fragment was opened rom search/favorites
     */
    private void getDataFromArguments() {
        Bundle bundle = getArguments();
        placeID = bundle.getString("place_id");
        name = bundle.getString("place_name");
        types = bundle.getString("place_type");
        address = bundle.getString("place_address");
        rating = bundle.getDouble("place_rating");
        rating = bundle.getDouble("place_rating");
        lat = bundle.getDouble("place_lat");
        lng = bundle.getDouble("place_lng");
        iconUrl = bundle.getString("place_icon_url");
        isOpen = bundle.getInt("place_is_open");
        isFavorite = bundle.getBoolean("place_is_favorite");
        mLocation = bundle.getParcelable("current_location");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_place_detail, container, false);
        findViews(v);
        setOnClickListeners();
        setData();
        if (!Utility.isTwoPaneMode(getActivity())) {
            initMapFragment();
        }
        return v;
    }

    /**
     * finds by ID all views
     *
     * @param v the root view to look in
     */
    private void findViews(View v) {
        placePhoto = (ImageView) v.findViewById(R.id.place_detail_photo);
        placeNameTV = (TextView) v.findViewById(R.id.place_name);
        placeTypeTV = (TextView) v.findViewById(R.id.place_type);
        placeAddressTV = (TextView) v.findViewById(R.id.place_address);
        websiteTV = (TextView) v.findViewById(R.id.website_action);
        shareTV = (TextView) v.findViewById(R.id.share_action);
        directionsTV = (TextView) v.findViewById(R.id.directions_action);
        callTV = (TextView) v.findViewById(R.id.call_action);
        distanceTV = (TextView) v.findViewById(R.id.distance);
        isOpenTV = (TextView) v.findViewById(R.id.is_open);
        ratingTv = (TextView) v.findViewById(R.id.rating);
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        favoriteFab = (FloatingActionButton) v.findViewById(R.id.favorite_fab);
    }

    /**
     * displays data
     */
    private void setData() {
        Picasso.with(getActivity()).load(iconUrl)
                .placeholder(R.drawable.place_holder)
                .into(placePhoto);
        if (toolbar != null) {
            toolbar.setTitle(name);
        }
        placeNameTV.setText(name);
        placeTypeTV.setText(types);
        placeAddressTV.setText(address);
        if (mLocation != null) {
            double distance = Utility.calculateDistance(mLocation.getLatitude(), mLocation.getLongitude(), lat, lng);
            String finalDistance = Utility.changeDistanceUnits((float) distance, getActivity());
            distanceTV.setText(finalDistance);
        } else {
            distanceTV.setText("N/A");
        }
        if (isOpen == 1) {
            isOpenTV.setText(R.string.open);
            isOpenTV.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_green_light));
        } else {
            isOpenTV.setText(R.string.closed);
            isOpenTV.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_light));
        }
        ratingTv.setText(Double.toString(rating));
        if (isFavorite) {
            favoriteFab.setImageResource(R.drawable.ic_favorite_white_24dp);
        } else {
            favoriteFab.setImageResource(R.drawable.ic_favorite_border_white_24dp);
        }
    }

    /**
     * creates map fragment with location
     */
    private void initMapFragment() {
        MapFragment mapFragment = new MapFragment();
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng))
                        .title(name).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_action_location_on)));
                marker.showInfoWindow();
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(lat, lng))// Sets the center of the map to location user
                        .zoom(15)// Sets the zoom
                        .bearing(0)// Sets the orientation of the camera to east
                        .tilt(0)// Sets the tilt of the camera to 30 degrees
                        .build();// Creates a CameraPosition from the builder
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);

            }
        });
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.map_container_place_detail, mapFragment).commit();
    }

    private void findPlacesById() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GooglePlacesApi googlePlacesApi = retrofit.create(GooglePlacesApi.class);
        Call<ResponseBody> call = googlePlacesApi.getPlaceById(placeID, "AIzaSyCgKsjYNr1jQWL2rPhzK7Fr6uZW-D8k6IU");
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
            ResponseBody res = response.body();
            if (res != null) {
                try {
                    JSONObject reports = new JSONObject(res.string());
                    JSONObject result = reports.getJSONObject("result");
                    phone = result.getString("formatted_phone_number");
                    website = result.getString("website");
                    if(isAttached){
                        setData();
                        if (!Utility.isTwoPaneMode(getActivity())) {
                            initMapFragment();
                        }
                    }
                    setOnClickListeners();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
    }

    /**
     * sets on click listeners
     */
    private void setOnClickListeners() {
        websiteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(website != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(website));
                    startActivity(i);
                }else{
                    showDialog(getString(R.string.no_website_available));
                }
            }
        });

        shareTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fixText = Uri.encode(name);
                String url = "https://maps.google.com/?q=" + fixText + "&ll=" + lat + "," + lng + "&z=18";
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                Intent chooser = Intent.createChooser(sendIntent, getString(R.string.choos_app));
                sendIntent.putExtra(Intent.EXTRA_TEXT, url);
                sendIntent.setType("text/plain");
                startActivity(chooser);
            }
        });

        directionsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:" + lat + "," + lng));
                startActivity(intent);
            }
        });

        callTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phone != null) {
                    Intent call = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        String[] perms = new String[]{"android.permissions.CALL_PHONE"};
                        int responseCode = 200;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(perms, responseCode);
                        }
                    }
                    startActivity(call);
                }else{
                    showDialog(getString(R.string.no_phone_available));
                }
            }
        });

        favoriteFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    int isF;
                    if (isFavorite) {
                        isFavorite = false;
                        favoriteFab.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                        isF = 0;
                    } else {
                        isFavorite = true;
                        favoriteFab.setImageResource(R.drawable.ic_favorite_white_24dp);
                        isF = 1;
                    }
                if (Utility.isAlreadyExistsInDataBase(placeID, getActivity())) {
                    ContentValues values = new ContentValues();
                    values.put(PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE, isF);
                    getActivity().getContentResolver().update(PlacesContract.SearchResultsEntry.CONTENT_URI, values, PlacesContract.SearchResultsEntry.COLUMN_PLACE_ID + " = ?", new String[]{placeID});
                } else {
                    ContentValues cv = new ContentValues();
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_ID, placeID);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_NAME, name);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_TYPE, types);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_ADDRESS, address);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_ICON_REFERENCE, iconUrl);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_PLACE_IS_OPEN, 0);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_RATING, rating);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_COORD_LAT, lat);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_COORD_LONG, lng);
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_IS_FAVORITE, Integer.toString(isF));
                    cv.put(PlacesContract.SearchResultsEntry.COLUMN_IS_HISTORY, "1");
                    getActivity().getContentResolver().insert(PlacesContract.SearchResultsEntry.CONTENT_URI, cv);
                }
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        isAttached = true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        isAttached = false;
    }

    void showDialog(String title) {
        DialogFragment newFragment = MyAlertDialogFragment.newInstance(
                title);
        newFragment.show(getFragmentManager(), "dialog");
    }
}


