package com.einavapps.mapfragmentapp.fragments;


import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.einavapps.mapfragmentapp.R;
import com.einavapps.mapfragmentapp.common.Utility;
import com.einavapps.mapfragmentapp.controler.FavoritesRecyclerAdapter;
import com.einavapps.mapfragmentapp.data.PlacesContract;
import com.einavapps.mapfragmentapp.intefaces.SetSearchFragment;


public class FavoritesFragment extends PlacesBaseListFragment implements FavoritesRecyclerAdapter.FavoriteClickCallback {

    private RelativeLayout noFavoritesMessage;
    private boolean menuVisible;


    @Override
    public void additionalMethodsInOnCreateView(View v, Bundle savedInstanceState) {
        noFavoritesMessage = (RelativeLayout) v.findViewById(R.id.no_favorites_message);
        findAndInitSearchTextButton(v);
    }

    @Override
    public RecyclerView findRecyclerView(View v) {
        return (RecyclerView) v.findViewById(R.id.recyclerview);
    }

    @Override
    public View inflateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.favorite_fragment, container, false);
    }

    @Override
    public Uri buildUri() {
        return PlacesContract.SearchResultsEntry.buildFavoritesUri();
    }

    @Override
    public void onCursorReceived(Cursor data) {
        if (data.moveToFirst()) {
            noFavoritesMessage.setVisibility(View.GONE);
            if (isFirstInitializationOfRecyclerView) {
                int numOfColumns = 2;
                boolean isTwoPane = Utility.isTwoPaneMode(getActivity());
                if (isTwoPane) {
                    numOfColumns = 1;
                }
                recView.setLayoutManager(new GridLayoutManager(getActivity(), numOfColumns));
                mAdapter = new FavoritesRecyclerAdapter(data, getActivity(), null, this, this);
                recView.setAdapter(mAdapter);

                isFirstInitializationOfRecyclerView = false;
            } else {
                if (!menuVisible || getActivity().getFragmentManager().findFragmentById(R.id.main_container) != null) {
                    mAdapter.swapArrayList(Utility.cursorToArrayList(data));
                }
            }
        } else {
            if(!isFirstInitializationOfRecyclerView){
            }
            noFavoritesMessage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void additionalMethodsInOnLoaderReset() {

    }

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    private void findAndInitSearchTextButton(View v) {
        TextView searchForSomethingTv;
        searchForSomethingTv = (TextView) v.findViewById(R.id.search_text_button);
        searchForSomethingTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetSearchFragment activity = (SetSearchFragment) getActivity();
                activity.showSearchFragment();
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        this.menuVisible = menuVisible;
        super.setMenuVisibility(menuVisible);
    }

    @Override
    public void showNoFavoritesMessage() {
        Animation alphaAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.alpha_anim);
        alphaAnimation.setDuration(700);
        noFavoritesMessage.setVisibility(View.VISIBLE);
        noFavoritesMessage.setAnimation(alphaAnimation);
    }
}
